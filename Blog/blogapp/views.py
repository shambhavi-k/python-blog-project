from django.shortcuts import render,get_object_or_404
from blogapp.models import Post
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from blogapp.forms import CommentsForm
from taggit.models import Tag
# Create your views here.
def post_list(request,tag_slug=None):
    post_list=Post.objects.filter(status__exact='published')
    tag=None
    if tag_slug:
        tag=get_object_or_404(Tag,slug=tag_slug)
        post_list=post_list.filter(tags__in=[tag])
    paginator=Paginator(post_list,3)
    page_number=request.GET.get('page')#page is predefined word
    try:
        post_list=paginator.page(page_number)
    except PageNotAnInteger:
        post_list=paginator.page(1)
    except EmptyPage:
        post_list=paginator.page(paginator.num_pages)
    return render(request,'blogapp/post_list.html',{'post_list':post_list,'tag':tag})

def post_detail(request,year,month,day,post):
    post=get_object_or_404(Post,status='published',publish__year=year,publish__month=month,publish__day=day)
    comments=post.comm.filter(active=True)
    csubmit=False
    if request.method=='POST':
        form=CommentsForm(request.POST)
        if form.is_valid():
            new_comment=form.save(commit=False)
            new_comment.post=post
            new_comment.save()
            csumbit=True
    else:
        form=CommentsForm()
    return render(request,'blogapp/post_detail.html',{'post':post,'form':form,'csubmit':csubmit,'comments':comments})


#Email Module
from django.core.mail import send_mail
from blogapp.forms import EmailSendingForm
def mail_sending_view(request,id):
    post=get_object_or_404(Post,id=id,status='published')
    sent=False
    if request.method=='POST':
        form=EmailSendingForm(request.POST)
        if form.is_valid():
            cd=form.cleaned_data
            subject='{}({}) recommends you to read "{}"'.format(cd['name'],cd['email'],post.title)
            post_url=request.build_absolute_uri(post.get_absolute_url())
            message='Read Post At:\n{}\n\n{}\'s comments:\n{}'.format(post_url,cd['name'],cd['comments'])
            send_mail(subject,message,'SK@blog.com',[cd['to']])
            sent=True
    else:
        form=EmailSendingForm()
    return render(request,'blogapp/mail.html',{'form':form,'post':post,'sent':sent})
