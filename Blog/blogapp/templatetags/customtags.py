from blogapp.models import Post
from django import template
from django.db.models import Count
register=template.Library()

@register.simple_tag
def totalposts():
    #totalPosts is a name of templatetags
    return Post.objects.count()

@register.inclusion_tag('blogapp/latestpost.html')
def show_latestpost(count=5):
    latestpost=Post.objects.order_by('-publish')[:count]
    return {'latestpost':latestpost}

@register.simple_tag
def get_most_commentedposts():
    return Post.objects.annotate(total_comments=Count('comm')).order_by('-total_comments')[:4]
