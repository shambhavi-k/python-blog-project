from django.contrib import admin
from blogapp.models import Post
from blogapp.models import Comments
# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_display=['title','slug','author','body','publish','created','updated','status']
    list_filter=('status','author')
    search_fields=('title','body')
    raw_id_fields=('author',)
    ordering=('created',)
    prepopulated_fields={'slug':('title',)}

class CommentAdmin(admin.ModelAdmin):
    list_display=('id','name','email','post','body','created','updated','active')
    list_filter=('active','created','updated')
    serach_fields=('name','email','body')

admin.site.register(Post,PostAdmin)
admin.site.register(Comments,CommentAdmin)
